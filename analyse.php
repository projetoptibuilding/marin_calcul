<!--
Cette page présente un détail des résultats des calculs de coûts
-->

<!DOCTYPE html>
<html>
<head>
    <title>OptiBuilding</title>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="DesignOptibuilding.css"/>
</head>
        
<body>
<?php

        $id_projet=htmlentities($_GET['projet']);
        $projet="projet_".$id_projet;
        $id_piece=htmlentities($_GET['piece']);;
        $id_scenario=htmlentities($_GET['scenario']);
        
        require("fonctions_cout_global.php");
        
        try {$bdd= new PDO ('mysql:host=localhost;dbname='.$projet.';charset=utf8', 'root', '',
                                   array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
        catch (Exception $e)
                {die('Erreur : ' . $e->getMessage());}
                
        $req=$bdd->query('SELECT * FROM informations');
        $donnes=$req->fetch();
        
        $duree_exp=$donnes['duree_exploitation'];
        $taux=$donnes['taux_inflation'];
?>


<p><table>
    <caption>Coût de remplacement</caption>
        <thead>
                <tr>
                    <th></th>
                    <th colspan=2> Coût de Remplacement</th>
                    <th></th>
                </tr>
                <tr>
                    <th>Année</th>
                    <th>Brut</th>
                    <th>Courant</th>
                    <th>Matériaux</th>
                </tr>
        </thead>
        
        <tbody>
                
<?php  $tableau=replace_cost_vector ($projet,$id_piece,$id_scenario,$duree_exp);
        
        foreach ($tableau as $key => $value){
                if($value!=0){
                        $n=$key;
?>
                <tr>
                        <td><?php echo $key+1;?></td>
                        <td><?php echo number_format(sprintf('%.2f',$value),2,"."," ");?> €</td>
                        <td><?php echo number_format(sprintf('%.2f',$value*pow((1+$taux/100),$key)),2,"."," ");?> €</td>
                        <td>
                <?php $req1=$bdd->query('SELECT * FROM articles WHERE id_scenario="'.$id_scenario.'" AND id_piece="'.$id_piece.'"');
                                        while($donnes1=$req1->fetch())
                {
                        if(condition_replace($donnes1['duree_de_vie'],$key+1,$duree_exp)==1)
                                {
                                $cout=$donnes1['taux_remplacement']*$donnes1['prix_unitaire']*$donnes1['surface'];
                                $cout_courant=$cout*pow((1+$taux/100),$key);
                                ?>
                                <a href='../articles/fiche_article.php?projet=<?php echo $id_projet;?>&piece=<?php echo $id_piece;?>&scenario=<?php echo $id_scenario;?>&mat=<?php echo $donnes1['code_article'];?>'>
                                <?php echo $donnes1['code_article'];?>
                                </a>
                                <?php echo '   '.'('.number_format(sprintf('%.2f',$cout),2,"."," ").' € || '.number_format(sprintf('%.2f',$cout_courant),2,"."," ").' €)   ; ';
                                }
                }
                        ?>
                        </td>
                </tr>
<?php
                }
         }
?>
        </tbody>
</table></p>

</br></br>

    <p><table>
    <caption>Analyse par matériau</caption>
        <thead>
                <tr>
                    <th></th>
                    <th></th>
                    <th colspan=3>Brut</th>
                    <th colspan=3>Courant</th>
                </tr>
                <tr>
                    <th>Matériau</th>
                    <th>Construction</th>
                    <th>Maintenance</th>
                    <th>Remplacement</th>
                    <th>Total</th>
                    <th>Maintenance</th>
                    <th>Remplacement</th>
                    <th>Total</th>
                </tr>
        </thead>
        <tbody>
<?php $req3=$bdd->query('SELECT * FROM articles WHERE id_scenario="'.$id_scenario.'" AND id_piece="'.$id_piece.'" ORDER BY code_article');
        while($donnes3=$req3->fetch())
                {
                    $construction=number_format(sprintf('%.2f',$donnes3['prix_unitaire']*$donnes3['surface']),2,"."," ");
                    $maintenance=number_format(sprintf('%.2f',$donnes3['taux_entretien']*$donnes3['prix_unitaire']*$donnes3['surface']),2,"."," ");
                    $maintenance_total=$maintenance*$duree_exp;
                    $nb=number_replace($donnes3['duree_de_vie'],$duree_exp);
                    //number_replace est une fonction que j'ai créé
                    $rempl=$donnes3['taux_remplacement']*$donnes3['prix_unitaire']*$donnes3['surface'];
                    $remplace=number_format(sprintf('%.2f',$rempl),2,"."," ");
                    $rempl_t=$nb*$rempl;
                    $remplace_total=number_format($rempl_t,2,"."," ");
                    $total=number_format($construction+$maintenance_total+$remplace_total,2,"."," ");
                    ?>
            <tr>
                <td><a href='../articles/fiche_article.php?projet=<?php echo $id_projet;?>&piece=<?php echo $id_piece;?>&scenario=<?php echo $id_scenario;?>&mat=<?php echo$donnes3['code_article'] ;?>'><?php echo$donnes3['code_article'] ;?></a></td>
                <td><?php   echo $construction;?> €</td>
                <!-- Couts bruts-->
                <td><?php   echo $duree_exp;?> x <?php echo $maintenance;?> = <?php echo $maintenance_total;?> €</td>
                <td><?php   echo $nb;?> x <?php echo $remplace;?> = <?php echo $remplace_total;?> €</td>
                <td><?php   echo $total;?> €</td>
                
                <!-- Couts courants-->
                <td><?php   $maitenance_total_courant=$maintenance;
                            for($i=0;$i<$duree_exp;$i++){
                            $maitenance_total_courant+=$maintenance*pow((1+$taux/100),$i);}
                            echo number_format($maitenance_total_courant,2,"."," ");?> €
                </td>
                <td><?php   $remplace_total_courant=$remplace_total;
                            for($i=0;$i<$duree_exp;$i++){
                            $remplace_total_courant+=$remplace*pow((1+$taux/100),$i);}
                            echo number_format($remplace_total_courant,2,"."," ");?> €
                </td>
                <td><?php   $total_courant=$construction+$maitenance_total_courant+$remplace_total_courant;
                            echo number_format($total_courant,2,"."," ");?> €
                </td>
            </tr>
<?php }?>
        </tbody>
        </table></p>
              
    <p><a href='calcul_cout_global.php?projet=<?php echo $id_projet;?>&piece=<?php echo $id_piece;?>&scenario=<?php echo $id_scenario;?>' class='icones left_arrow'> Retour</a></p>
        
</body>
</html>