<!--
Cette page permet la sauvegarde des résultats des calculs de coûts. Les résultats sont stockés dans la table 'resultats'
-->

<?php
    $id_projet=htmlentities($_POST['projet']);
    $projet="projet_".$id_projet;
    $id_piece=htmlentities($_POST['piece']);
    $id_scenario=htmlentities($_POST['scenario']);
        
    try {$bdd= new PDO ('mysql:host=localhost;dbname='.$projet.';charset=utf8', 'root', '',
                                       array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
    catch (Exception $e)
        {die('Erreur : ' . $e->getMessage());}
    
    // IL faut au préalable que l'on insère une ligne en créant le scénario

    $req=$bdd->prepare('UPDATE resultats
                       SET
                       cout_construction=:cout_construction,
                       cout_remplacement=:cout_remplacement, cout_maintenance=:cout_maintenance, cout_global=:cout_global,
                       cout_remplacement_courant=:cout_remplacement_courant, cout_maintenance_courant=:cout_maintenance_courant,
                       cout_global_courant=:cout_global_courant
                       WHERE id_piece=:id_piece AND id_scenario=:id_scenario');
    //modifier l'id du scenario et de la piece dans la requête suivante                   
    $req->execute(array('id_piece'=>$id_piece, 'id_scenario'=>$id_scenario, 'cout_construction'=>htmlentities($_POST['cout_construction']),
                        'cout_remplacement'=>htmlentities($_POST['cout_remplacement']), 'cout_maintenance'=>htmlentities($_POST['cout_maintenance']),
                        'cout_global'=>htmlentities($_POST['cout_global']),
                        'cout_remplacement_courant'=>htmlentities($_POST['cout_remplacement_courant']),'cout_maintenance_courant'=>htmlentities($_POST['cout_maintenance_courant']),
                        'cout_global_courant'=>htmlentities($_POST['cout_global_courant'])
                        ));              
    
    header('Location:calcul_cout_global.php?projet='.$id_projet.'&piece='.$id_piece.'&scenario='.$id_scenario.'');
?>