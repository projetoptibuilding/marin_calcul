<!--
Cette page présente les différents résultats des calculs, elle permet d'accèder à la table des articles et à la page d'analyse des résultats
-->

<?php

        $id_projet=htmlentities($_GET['projet']);
        $projet="projet_".$id_projet;
        $id_piece=htmlentities($_GET['piece']);
        $id_scenario=htmlentities($_GET['scenario']);
        
        
if(       isset($_POST['taux_inflation']) && isset($_POST['duree_exp']) &&
          isset($_POST['autre_annuel']))
    {  
        $duree_exp=htmlentities($_POST['duree_exp']);
        $autre=htmlentities($_POST['autre']);
        $autre_annuel=htmlentities($_POST['autre_annuel']);
        $taux_inflation=htmlentities($_POST['taux_inflation']);
        $coeff_inflation=1+$taux_inflation/100;
        $coeff_nul=1;
        $nul=0;
        
        $externalities=htmlentities($_POST['cout_externalite']);
        $externalities_courant=htmlentities($_POST['cout_externalite_courant']);
        
        try {$bdd= new PDO ('mysql:host=localhost;dbname='.$projet.';charset=utf8', 'root', '',
                               array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
        catch (Exception $e)
                    {die('Erreur : ' . $e->getMessage());}
                    
        $req=$bdd->query('SELECT id_article, surface, prix_unitaire, duree_de_vie, taux_entretien, taux_remplacement FROM articles WHERE id_piece="'.$id_piece.'" AND id_scenario="'.$id_scenario.'" ORDER BY id_article');

        // On fait appel aux fonctions du fichier fonctions_cout_global.php
        require("fonctions_cout_global.php");
        
        $replace_cost=sprintf('%.2f',replace_cost($projet,$id_piece,$id_scenario,$duree_exp,$coeff_nul)); 
        $replace_cost_courant=sprintf('%.2f',replace_cost($projet,$id_piece,$id_scenario,$duree_exp,$coeff_inflation)); 

        $construction_cost=sprintf('%.2f',construction_cost($projet,$id_piece,$id_scenario));
        
        $maintenance_cost=sprintf('%.2f',maintenance_cost($projet,$id_piece,$id_scenario,$duree_exp,$coeff_nul));
        $maintenance_cost_courant=sprintf('%.2f',maintenance_cost($projet,$id_piece,$id_scenario,$duree_exp,$coeff_inflation));
        
        $cout_global=sprintf('%.2f',Cout_Global($projet,$id_piece,$id_scenario, $duree_exp, $coeff_nul, $nul));
        
        $cout_global_courant=sprintf('%.2f',Cout_Global_Courant($projet,$id_piece,$id_scenario, $duree_exp, $coeff_inflation, $nul));
        
    
    }else{
        try {$bdd= new PDO ('mysql:host=localhost;dbname='.$projet.';charset=utf8', 'root', '',
                               array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
        catch (Exception $e)
                    {die('Erreur : ' . $e->getMessage());}
                    
        $req=$bdd->query('SELECT * FROM resultats WHERE id_piece='.$id_piece.' AND id_scenario='.$id_scenario.' ');
        $donnes=$req->fetch();
        
        $replace_cost=$donnes['cout_remplacement'];
        $replace_cost_courant=$donnes['cout_remplacement_courant'];
        $construction_cost=$donnes['cout_construction'];       
        $maintenance_cost=$donnes['cout_maintenance'];
        $maintenance_cost_courant=$donnes['cout_maintenance_courant'];
        $cout_global=$donnes['cout_global'];       
        $cout_global_courant=$donnes['cout_global_courant'];       
                    
        $req2=$bdd->query('SELECT * FROM informations');
        $donnes2=$req2->fetch();
        
        $taux_inflation=$donnes2['taux_inflation'];
        $duree_exp=$donnes2['duree_exploitation'];
        $autre=$donnes2['cout_ext_fixe'];
        $autre_annuel=$donnes2['cout_ext_annuel'];
        $externalities=$donnes2['cout_externalite'];
        $externalities_courant=$donnes2['cout_externalite_courant'];
    }
        ?>

<!DOCTYPE html>
<html>
<head>
    <title>OptiBuilding</title>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="DesignOptibuilding.css"/>
</head>
        
<body>
     <p><a href='../articles/table_articles.php?projet=<?php echo $id_projet;?>&piece=<?php echo $id_piece;?>&scenario=<?php echo $id_scenario;?>'>
     <input class='icones' type='button' value='Table des articles'/></a></p>
     
     <p><a href='../infos_projet.php?projet=<?php echo $id_projet;?>&piece=<?php echo $id_piece;?>&scenario=<?php echo $id_scenario;?>'>
     <input class='icones' type='button' value='Informations sur le projet'/></a></p>

     <p><fieldset>Informations sur le projet
        <fieldset>
            <legend>Taux</legend>
            Taux d'inflation : <?php if(isset($taux_inflation)){echo number_format($taux_inflation,2,"."," ");}else{echo 2.0;}?> %</br>
            Durée d'exploitation : <?php if(isset($duree_exp)){echo $duree_exp;}else{echo 0;}?> ans</br>
        </fieldset>
        
        <fieldset>
            <legend>Externalités</legend>
            Coûts fixes : <?php if(isset($autre)){echo number_format($autre,2,"."," ");}else{echo 0;}?> €</br>
            Coûts annuels : <?php if(isset($autre_annuel)){echo number_format($autre_annuel,2,"."," ");}else{echo 0;}?> €</br>
            </br>
            Coût des externalités : <?php echo number_format($externalities,2,"."," ");?> €</br>
            Coût courant des externalités : <?php echo number_format($externalities_courant,2,"."," ");?> €</br>
        </fieldset>
        </fieldset></p>
        
        <form method="post" action="">
            <input type='hidden' name='projet' value="<?php echo $id_projet;?>"/>
            <input type='hidden' name='piece' value='<?php echo $id_piece;?>'/>
            <input type='hidden' name='scenario' value='<?php echo $id_scenario;?>'/>
            <input type='hidden' name='taux_inflation' value='<?php echo $taux_inflation;?>'/>
            <input type='hidden' name='duree_exp' value='<?php echo $duree_exp;?>'/>
            <input type='hidden' name='autre' value='<?php echo $autre;?>'/>
            <input type='hidden' name='autre_annuel' value='<?php echo $autre_annuel;?>'/>
            <input type='hidden' name='cout_externalite' value='<?php echo $externalities;?>'/>
            <input type='hidden' name='cout_externalite_courant' value='<?php echo $externalities_courant;?>'/>
        
        <p><input  type='submit' value='Calculer' class='icones calculer'/></p>
        </form>
        
     
     
    <form method="post" action="sauvegarde_resultats.php">
        <input type='hidden' name='projet' value='<?php echo $id_projet; ?>'/>
        <input type='hidden' name='piece' value='<?php echo $id_piece; ?>'/>
        <input type='hidden' name='scenario' value='<?php echo $id_scenario; ?>'/>
        <input type='hidden' name='taux_inflation' value='<?php echo $taux_inflation;?>'/>
        <input type='hidden' name='duree_exploitation' value='<?php echo $duree_exp; ?>'/>
        <input type='hidden' name='cout_ext_fixe' value='<?php echo $autre; ?>'/>
        <input type='hidden' name='cout_ext_annuel' value='<?php echo $autre_annuel; ?>'/>
        <input type='hidden' name='cout_construction' value='<?php echo $construction_cost; ?>'/>
        <input type='hidden' name='cout_remplacement' value='<?php echo $replace_cost; ?>'/>
        <input type='hidden' name='cout_maintenance' value='<?php echo $maintenance_cost; ?>'/>
        <input type='hidden' name='cout_externalites' value='<?php echo $externalities; ?>'/>
        <input type='hidden' name='cout_global' value='<?php echo $cout_global; ?>'/>
        <input type='hidden' name='cout_remplacement_courant' value='<?php echo $replace_cost_courant; ?>'/>
        <input type='hidden' name='cout_maintenance_courant' value='<?php echo $maintenance_cost_courant; ?>'/>
        <input type='hidden' name='cout_externalites_courant' value='<?php echo $externalities_courant; ?>'/>
        <input type='hidden' name='cout_global_courant' value='<?php echo $cout_global_courant; ?>'/>
        <input type='submit' value='Enregistrer les résultats' class='icones'/>
    </form>
     
     
     <p>
        <p>Pour ce scénario :</p>
        
        <p><a href='analyse.php?projet=<?php echo $id_projet;?>&piece=<?php echo $id_piece;?>&scenario=<?php echo $id_scenario;?>'>
        <input type='button' value='Analyser les coûts' class='icones'/></a></p>
        
        Coût de construction : <?php echo number_format($construction_cost,2,"."," ");?> €</br></br> 
        
        Coût de remplacement : <?php echo number_format($replace_cost,2,"."," ");?> €</br>
        Coût de maintenance : <?php echo number_format($maintenance_cost,2,"."," ");?> €</br>
        </br>
        Coût global : <?php echo number_format($cout_global,2,"."," ");?> €</br>
        </br></br>
        Coût de remplacement : <?php echo number_format($replace_cost_courant,2,"."," ");?> €</br>
        Coût de maintenance : <?php echo number_format($maintenance_cost_courant,2,"."," ");?> €</br>
        </br>
        Coût global courant : <?php echo number_format($cout_global_courant,2,"."," ");?> €</br>
        </br></br>
     </p>
     
</body>
</html>